<?php

namespace App\Models;

use CodeIgniter\Model;
use JeroenDesloovere\VCard\VCard;

class Nomina extends Model
{
    protected $DBGroup          = 'nomina';
    protected $table            = 'tnomina';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        //'id',
        'n_legajo',
        'x_hash',
        'x_nombres',
        'x_apellidos',
        'x_email',
        'x_sector',
        'x_cargo',
        'x_movil',
        'x_telefono',
        'x_direccion',
        'x_linkedin',
        'x_direccion',
        'm_sent'
        //'created_at',        
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = '';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [
        'createHash',
    ];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    /**
     * createHash
     * 
     * Dado un registro, crea un hash "unico" (sha1) en base al legajo
     *
     * @param array $data el registro propiamente dicho
     * @return void
     */
    protected function createHash( array $data) {
        $seed = [
            'n_legajo' => $data['data']['n_legajo'],
        ];
        $data['data']['x_hash'] = sha1(serialize($seed));
        return $data;
    }

    /**
     * addVCard
     * 
     * Dado un registro, devuelve el string correspondiente a la VCard
     *
     * @return void
     */
    public function addVCard($data) {

        // TODO: el find by id trae los datos d euna manera distinta

        $cfg= Config('VCard');

        for ( $i=0 ; $i< count($data['data']) ; $i++){
            $vcard = new VCard();

            $vcard->addName($data['data'][$i]['x_apellidos'],$data['data'][$i]['x_nombres']);

            $vcard->addCompany($cfg->companyName);
            $vcard->addJobtitle($data['data'][$i]['x_cargo']);
            $vcard->addEmail($data['data'][$i]['x_email']);
            $vcard->addPhoneNumber($data['data'][$i]['x_movil']);
            $vcard->addPhoneNumber($data['data'][$i]['x_telefono']);
            $vcard->addAddress(null, null, $data['data'][$i]['x_direccion']);
            $vcard->addURL($cfg->companyURL);

            // Campos virtuales
            $data['data'][$i]['x_vcard'] = $vcard->getOutput();
            $data['data'][$i]['x_vcard_url'] = base_url() . '/' . $data['data'][$i]['x_hash'];
            $data['data'][$i]['x_image_qr'] =  $data['data'][$i]['n_legajo'] . '.png';
        }
        return $data; 
    }

    /**
     * getCSVHeaders
     * 
     * Devuelve los headers del CSV a exportar
     *
     * @return void
     */
    public static function getCSVHeaders() {
        return [
            'x_nombres',
            'x_apellidos',
            'x_email',
            'x_cargo',
            'x_movil',
            'x_telefono',
            'x_direccion',
            'x_linkedin',
            'x_direccion',
            'x_imagen_qr',
        ];
    }
}
