<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\Events\Events;

class Initial extends Seeder
{
    public function run()
    {
        $users = auth()->getProvider();

        $user = new User([
            'username' => 'qrtarjetas-admin',
            'email'    => 'qrtarjetas-admin@laholando.com',
            'password' => '1234',
        ]);
        $users->save($user);

    }
}
