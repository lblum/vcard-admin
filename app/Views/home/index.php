<?= $this->extend('base/main.php'); ?>

<?= $this->section('content'); ?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Bienvenido al administrador de tarjetas QR</h1>
</div>

<!-- Content Row -->
<div class="row">
    <!--div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center" onclick="window.location.replace('<?= base_URL() ?>/users');" style="cursor: pointer">
                    <div class="col mr-2">
                        <div class="h5 mb-0 font-weight-bold text-gray-800">Usuarios</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-person fa-2x"></i>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center" onclick="window.location.replace('<?= base_URL() ?>/nomina');" style="cursor: pointer">
                    <div class="col mr-2">
                        <div class="h5 mb-0 font-weight-bold text-gray-800">N&oacute;mina</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-table fa-2x"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>