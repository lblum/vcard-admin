<form class="row g-3 needs-validation" novalidate id="main-form">
<div class="modal fade" id="main-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h3 class="modal-title" id="agendaLabel">Nomina</h3>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-n_legajo' class='col-form-label'>Legajo:</label>
              <input type='text' class='form-control' placeholder='Legajo' id='main-form-n_legajo' required>
              <div class="invalid-feedback">
                Por favor ingrese el legajo
              </div>              
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_nombres' class='col-form-label'>Nombres:</label>
              <input type='text' class='form-control' placeholder='Nombres' id='main-form-x_nombres' required>
              <div class="invalid-feedback">
                Por favor ingrese los nombres
              </div>              
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_apellidos' class='col-form-label'>Apellidos:</label>
              <input type='text' class='form-control' placeholder='Apellidos' id='main-form-x_apellidos' required>
              <div class="invalid-feedback">
                Por favor ingrese los apellidos
              </div>              
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_email' class='col-form-label'>Email:</label>
              <input type='text' class='form-control' placeholder='Email' id='main-form-x_email' required>
              <div class="invalid-feedback">
                Por favor ingrese el email
              </div>              
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_cargo' class='col-form-label'>Cargo:</label>
              <input type='text' class='form-control' placeholder='Cargo' id='main-form-x_cargo'>
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_movil' class='col-form-label'>M&oacute;vil:</label>
              <input type='text' class='form-control' placeholder='M&oacute;vil' id='main-form-x_movil'>
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_telefono' class='col-form-label'>Tel&eacute;fono:</label>
              <input type='text' class='form-control' placeholder='Tel&eacute;fono' id='main-form-x_telefono'>
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_direccion' class='col-form-label'>Direcci&oacute;n:</label>
              <input type='text' class='form-control' placeholder='Direcci&oacute;n' id='main-form-x_direccion'>
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_linkedin' class='col-form-label'>Linkedin:</label>
              <input type='text' class='form-control' placeholder='Linkedin' id='main-form-x_linkedin'>
            </div>
          </div>
          <div class='col-sm-4'>
            <div class='form-group col-sm-12'>
              <label for='main-form-x_sector' class='col-form-label'>Sector:</label>
              <input type='text' class='form-control' placeholder='Sector' id='main-form-x_sector'>
            </div>
          </div>

        </div>
      </div>


      <div class="modal-footer justify-content-between" >
        <button type="button" class="btn btn-default"  data-bs-dismiss="modal" > Cancelar </button>

        <button style="display:none" type="submit" class="btn btn-success" id="edit-modal" onclick="editModal(event)">
          <span class="spinner-border spinner-border-sm stop-spinner" role="status" aria-hidden="true"></span>
          <i class="fa fa-save"></i>
          Guardar
        </button>

        <input type="hidden" id="operation" name="operation" value="" />

        <button type="button" class="btn btn-success" id="add-modal" onclick="addModal(event)">
          <span class="spinner-border spinner-border-sm stop-spinner" role="status" aria-hidden="true"></span>
          <i class="fa fa-save"></i>
          Guardar
        </button>
      </div>
    </div>
  </div>
</div>
</form>

<?= $this->include('nomina/nomina-modal-js.php'); ?>