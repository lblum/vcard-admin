<?= $this->extend('base/main'); ?>

<?= $this->section('content'); ?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Administraci&oacute;n de n&oacute;mina</h1>
</div>

<!-- Content Row -->
<div class="row">
    <table id="nominaTable" class="table-striped table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Legajo</th>
                <th>Empleado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
            <tr>
                <th>Legajo</th>
                <th>Empleado</th>
                <th>Acciones</th>
            </tr>
        </tfoot>
    </table>
</div>

<?= $this->include('nomina/nomina-modal'); ?>

<?= $this->endSection(); ?>

<?= $this->section('javascripts'); ?>

<?= $this->include('nomina/nomina-js'); ?>
<?= $this->endSection(); ?>