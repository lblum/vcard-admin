<script>
    var mainTable;
    var base_url = global_base_url + 'api/nomina';

    function addRec() {
        clearFields('main-form');
        $('#edit-modal').data('id', null);
        $('#main-form').removeClass('was-validated');
        $("#main-modal").modal({backdrop: 'static', keyboard: false});
        $("#main-modal").modal('show');
    }

    function editRec(id) {
        let ajax = showAjax(base_url,id);
        $('#edit-modal').data('id', id);
        $.ajax(ajax)
        .done(function(data) {
            recToModal('main-form',data);
            $('#main-form').removeClass('was-validated');
            $("#main-modal").modal({backdrop: 'static', keyboard: false});
            $("#main-modal").modal('show');
        }).fail(function(xhr) {
            console.log(xhr);

        }).always(function() {
            
        });        
    }

    function deleteRec(id) {
        swal.fire({
            title: "Borrar usuario",
            text: "Confirma el borrado",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si",
            cancelButtonText: "No"
        }).then(function(result) {
            if (result.value) {
                $.ajax(deleteAjax(base_url, id))
                    .done(function(resp) {
                        mainTable.ajax.reload();
                        snackOk('Registro borrado ok');
                    }).fail(function(xhr) {
                        console.log(xhr);
                        //showInfo(xhr.responseText);
                    });
            }
        });
    }
    

    $(document).ready(function() {
        mainTable = $('#nominaTable').DataTable({
            stateSave: true,
            language: dataTableLanguage,
            processing: true,
            dom: 'Bfrtip',
            buttons: [{
                text: '' +
                    '<i class="fa fa-plus"></i> ' +
                    'Agregar' +
                    '',
                action: function(e, dt, node, config) {
                    addRec();
                }
            }, ],
            ajax: indexAjax(base_url),
            order: [
                [0, 'asc']
            ],
            columns: [
                {
                    data: "n_legajo",
                },
                {
                    data: null,
                    render: function(data, type, row, meta) {
                        return `
                        ${row.x_apellidos},${row.x_nombres}
                        `
                    }
                },
                {
                    data: null,
                    className: "text-center",
                    render: function(data, type, row, meta) {
                        let buttons = 
`
<div class="btn-group" role="group" aria-label="Basic example">
    <button type="button" class="btn" onclick="editRec(${row.id})">
        <span class="menu-icon text-primary">
            <i class='mdi mdi-24px  mdi-note-edit'></i>
            <br/>
            Modificar
        </span>
    </button>
    <button type="button" class="btn" onclick="deleteRec(${row.id})">
        <span class="menu-icon" style="color:red">
            <i class='mdi mdi-24px mdi-close'></i>
            <br/>
            Borrar
        </span>
    </button>
    <button type="button" class="btn" onclick="sendMail(${row.id}}">
        <span class="menu-icon text-success">
            <i class='mdi mdi-24px  mdi-email'></i>
            <br/>
            Enviar Mail
        </span>
    </button>
</div>
`;
                        return buttons;
                    }
                },
             ]



        });
    });
</script>