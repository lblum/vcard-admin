<script>
function addModal(event) {
    $('#main-form').addClass('was-validated');

    
    if ( $('#main-form')[0].checkValidity() ) {
        saveData('add-modal');
    }

}

function saveData(spinner){
    
    var data = formToData('main-form');
    let id = $('#edit-modal').data('id');

    if (id) {
        ajax = updateAjax(base_url, id, data);
    } else {
        ajax = createAjax(base_url, data)
    }

    startSpinner(spinner)

    $.ajax(ajax)
    .done(function(data) {
        snackOk('Grabación OK');
        $("#main-modal").modal('hide');
        mainTable.ajax.reload();
    }).fail(function(xhr) {
        console.log(xhr);
        snackError('Error al cargar los registros.');
    }).always(function() {
        stopSpinner(spinner);
    });    
}

</script>