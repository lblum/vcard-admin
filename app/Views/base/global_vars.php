<script>
    let global_base_url = '<?= base_url() ?>';
    let global_jwt = '< ?= $jwt ?>';
    let global_user_id = '< ?= $user_id ?>';

    let dataTableLanguage = {
        "lengthMenu": "Mostrando _MENU_ registros por p&aacute;gina",
        "zeroRecords": "No se han encontrado resultados.",
        "info": "P&aacute;gina _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros Disponibles",
        "infoFiltered": "(filtrado de _MAX_ registros)",
        "sSearch": "Buscar",
        "oPaginate": {
            "sNext": "Pr&oacute;ximo",
            "sPrevious": "Anterior",
        },
        "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando ...</span> ',
    };

    let dataTableDom = '<"card"<"card-body"<"row align-items-middle"l<"ml-auto"f><"backButtonDiv"><"addButtonDiv">>>>' +
        '<"empty-card"><"card"<"card-body"tr' +
        '<"d-flex"p<"ml-auto"i>>>>';
    let simpleDataTableDom = '<"card"<"card-body"<"row align-items-middle"l>' +
        'tr' +
        '<"d-flex"p<"ml-auto"i>>>>';

</script>