<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_URL() ?>">
    <div class="sidebar-brand-icon">
        <i class="fas fa-qrcode"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Tarjetas QR</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider">

<!--li class="nav-item">
    <a class="nav-link" href="<?= base_URL() ?>/users">
        <i class="fas fa-fw fa-person"></i>
        <span>Usuarios</span></a>
</li-->

<li class="nav-item">
    <a class="nav-link" href="<?= base_URL() ?>/nomina">
        <i class="fas fa-fw fa-table"></i>
        <span>N&oacute;mina</span></a>
</li>

<hr class="sidebar-divider">

<li class="nav-item">
    <a class="nav-link">
        <i class="fas fa-key fa-fw"></i>
        Cambio de contrase&ntilde;a
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="<?= base_URL() ?>/logout">
        <i class="fas fa-sign-out-alt fa-fw "></i>
        <span>Salida</span>
    </a>
</li>

</ul>
<!-- End of Sidebar -->