<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        return view('home/index',$this->getData());
    }

    public function logout() {
        auth()->logout();
    }
}
