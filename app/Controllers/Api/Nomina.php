<?php

namespace App\Controllers\Api;

class Nomina extends BaseApiController
{
    protected $modelName = 'App\Models\Nomina';

    public function new() {
        return $this->respond(
            [
                "n_legajo" => 'legajo'
            ]
        );
    }
}
