<?php

namespace App\Controllers;

class NominaController extends BaseController
{
    public function index()
    {
        $model = new \App\Models\Nomina();
        $nomina = $model->findAll();
        return view('nomina/index.php',$this->getData(['nomina' => $nomina]));
    }
}
