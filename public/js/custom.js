/**
 * Arranca el spinner dentro de un boton
 * 
 * @param {*} spinnerId el id del spinner
 */
function startSpinner(spinnerId) {
    let spinner = $("#" + spinnerId + " span");
    spinner.removeClass('stop-spinner');
    spinner.addClass('busy-spinner');
}

/**
 * Detiene el spinner dentro de un boton
 * 
 * @param {*} spinnerId el id del spinner
 */

function stopSpinner(spinnerId) {
    let spinner = $("#" + spinnerId + " span");
    spinner.removeClass('busy-spinner');
    spinner.addClass('stop-spinner');
}

/**
 * timeout de autohide de los snacks (en milisegundos)
 */
var snackDelay = 5000;

/**
 * 
 * Muestra un snack de confirmación (success)
 * 
 * @param {*} msg 
 */
function snackOk(msg) {
    toastNotif({
        text: msg,
        color: '#5bc83f',
        timeout: snackDelay,
        icon: 'valid'
    });    
}

/**
 * 
 * Muestra un snack de error (danger)
 * 
 * @param {*} msg 
 */
function snackError(msg) {
    toastNotif({
        text: msg,
        color: '#da4848',
        timeout: snackDelay,
        icon: 'error'
    });    
}


/**
 * Configuración de los datetimepicker
 */
const confDataRange = {
    todayBtn: "linked",
    clearBtn: true,
    language: "es",
    autoclose: true,
    todayHighlight: true
};

/**
 * Funciones comunes a todos los crud
 */

/**
 * 
 * Elimina la barra final de una url
 * Esto es para evitar el problema del https y el .htaccess
 * 
 * @param {*} url 
 */
function noTrailingSlash(url) {
    return url.replace(/^(.+?)\/*?$/, "$1");
}

/**
 * 
 * Ajax para traer todos los registros
 * 
 * @param {*} base_url 
 * @returns 
 */
function indexAjax(base_url) {
    return {
        type: "GET",
        headers: {
            "Authorization": global_jwt,
        },
        url: noTrailingSlash(base_url)
    }
};


// Traer un registro especifico
/**
 * 
 * Ajax para traer un registro determinado
 * 
 * @param {*} base_url 
 * @param {*} id 
 * @returns 
 */
function showAjax(base_url, id) {
    return {
        type: "GET",
        headers: {
            "Authorization": global_jwt,
        },
        url: base_url + '/' + id
    };
}

/**
 * 
 * Ajax para insertar un registro
 * 
 * @param {*} base_url 
 * @param {*} data 
 * @returns 
 */
function createAjax(base_url, data) {
    return {
        type: "POST",
        headers: {
            "Authorization": global_jwt,
        },
        url: noTrailingSlash(base_url),
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
    };
}

/**
 * 
 * Ajax para actualizar un registro
 * 
 * @param {*} base_url 
 * @param {*} id 
 * @param {*} data 
 * @returns 
 */
function updateAjax(base_url, id, data) {
    return {
        type: "PUT",
        headers: {
            "Authorization": global_jwt,
        },
        url: base_url + '/' + id,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
    };
}

/**
 * 
 * Ajax para borrar un registro
 * 
 * @param {*} base_url la url de base
 * @param {*} id El id del registro
 * @returns 
 */
function deleteAjax(base_url, id) {
    return {
        type: "DELETE",
        headers: {
            "Authorization": global_jwt,
        },
        url: base_url + '/' + id,
    };
}

/**
 * Borra errores de validación de un form
 */
function clearErrors() {
    $(".is-invalid").each(function(index) {
        $(this).parent().find('span').remove();
        $(this).removeClass('is-invalid');
    });
}

/**
 * Si hay error 401, redirije al raíz
 * @param {*} prefix 
 * @param {*} xhr 
 */
function showErrorsX(prefix, xhr) {
    if (xhr.status == 401) {
        document.location.href = "/";
    }
    showErrors(prefix, xhr['responseJSON']);
}

/**
 * 
 * Muestro errores de validación de un form
 * 
 * @param {*} prefix El nombre del form
 * @param {*} errors El array de errores
 */
function showErrors(prefix, errors) {
    clearErrors();
    if (errors.error != undefined) {
        Swal.fire({
            position: 'top-center',
            icon: 'warning',
            title: 'Error',
            text: errors.error,
            showConfirmButton: true
        });
    } else {
        // Errores a setear en los campos
        errors.errors.forEach(function(item, index) {
            console.log(item.field, item.text);
            field = $("#" + prefix + " #" + prefix + "-" + item.field);
            field.addClass('is-invalid');
            field.parent().append('<span class="invalid-feedback">' + item.text + '</span>')
            console.log(field.length);
        });
    }
}

/**
 * 
 * Pasaje de los datos de un registro a un modal
 * 
 * @param {*} prefix 
 * @param {*} row 
 */
function recToModal(prefix, row) {
    for (item in row) {
        field = $("#" + prefix + " #" + prefix + "-" + item);
        if (field.length) {
            field.is(':checkbox') ? field.prop("checked", row[item] != 0) : field.val(row[item]);
        }
    }
}

/**
 * 
 * Pasaje de los datos de un modal a un registro
 * 
 * @param {*} prefix 
 * @param {*} row 
 */
function modalToRec(prefix) {
    // TODO: Tengo que revisar como hacerlo
    for (item in row) {
        field = $("#" + prefix + " #" + prefix + "-" + item);
        if (field.length) {
            field.is(':checkbox') ? field.prop("checked", row[item] != 0) : field.val(row[item]);
        }
    }
}

/**
 * Pasa una fecha a formato ANSI
 * 
 * @param {*} strDate 
 * @returns 
 */
function dateToAnsi(strDate) {
    var date = moment(strDate, "DD/MM/YYYY");
    if (!date.isValid())
        return "";
    return date.toJSON();
}

/**
 * Todas las variantes de nulo
 * @param {*} data 
 * @returns 
 */
function nullData(data) {
    if ( Number.isNaN(data) )
        return 0;
    return (data == null || data == undefined) ? "" : data;
}


function dateToDDMMYYY(strDate) {
    var date = moment(strDate, moment.ISO_8601);
    if (!date.isValid())
        return "";
    return date.format("DD/MM/YYYY");
}

function dateToDDMMYYYHHMMSS(strDate) {
    var date = moment(strDate, moment.ISO_8601);
    if (!date.isValid())
        return "";
    return date.format("DD/MM/YYYY HH:mm:ss");
}

/**
 * Pseudo-random string generator
 * http://stackoverflow.com/a/27872144/383904
 * Default: return a random alpha-numeric string
 * 
 * @param {Integer} len Desired length
 * @param {String} an Optional (alphanumeric), "a" (alpha), "n" (numeric)
 * @return {String}
 */
function randomString(len, an) {
    an = an && an.toLowerCase();
    var str = "",
        i = 0,
        min = an == "a" ? 10 : 0,
        max = an == "n" ? 10 : 62;
    for (; i++ < len;) {
        var r = Math.random() * (max - min) + min << 0;
        str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
    }
    return str;
}

function mainSpinnerOn() {
    $('#mainSpinner').modal('show');
 }

 function mainSpinnerOff(){
    $('#mainSpinner').modal('hide');
 }

 function validateForm(formId) {
    let form = document.getElementById(formId);
    if (!form.checkValidity()) {
        return false;
      }

      form.classList.add('was-validated');
      return true;
 }

 function clearFields(formId) {
     $(`#${formId} input`).each(function() {
        $(this).val('');
     });
 }

 function formToData(formId) {
    var retVal = {};
    $(`#${formId} input`).each(function() {
        let field = $(this);
        let fName = field.attr('id').replace(`${formId}-`,'');
        retVal[fName] = field.val();
     });

     return retVal;

 }

 function fieldsToForm(data,formId) {
    for ( const property in data ) {
        console.log(property);
    }
    
    /*
    $(`${formId} input`).each(function() {
       $(this).val('');
    });
    */
}